// Gameのすべてを管理するクラス
class Game {
  final String playerName;
  final String host;
  Program program;

  Game({ this.playerName, this.host }) {
    print('gameクラスをインスタンス化したよ');
  }

  void getProgram(){
    this.program = Program(host: this.host);
  }
}

// 問題についての管理はこちらのクラスに任せる
class Program {
  final String host;
  List programList;

  Program({ this.host }){
    print('Programクラスをインスタンス化したよ');
    print('$hostを利用してAPIを叩いて問題一覧を取得するよ。');
    print('この処理には時間がかかるよ。');
  }
}

void main() {
  // インスタンス化した時には問題は取得しない
  Game game = Game(playerName: 'プレイヤーA', host: 'https://XXXXX.XXX/');
  // 色々他の処理とかしてー
  print('今は別の処理をしているよ');
  // いざ問題を取得したい時にする
  game.getProgram();
}