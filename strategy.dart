// 問題クラス
class Program {
  int id;
  dynamic answer;
  Printer printer;

  Program({this.id, this.answer, this.printer});

  void answerString(){
    print(printer.doPrint(answer));
  }
}

// Printの抽象クラス
abstract class Printer {
  String doPrint(dynamic answer){
    return "";
  }
}

// String型を普通にString型で返すクラス
class PrintText implements Printer {
  String doPrint(dynamic answer){
    return answer;
  }
}

// List型をString型にして返すクラス
class PrintList implements Printer {
  String doPrint(dynamic answer){
    String text = "";

    for(var i in answer){
      text += '$i, ';
    }
    return text;
  }
}

void main() {
  // questionAの回答はString型
  Program questionA = Program(id: 1, answer: "答え", printer: PrintText());
  // questionBの回答はList型
  Program questionB = Program(id: 1, answer: ['A', 'B', 'C'], printer: PrintList());
  // どちらもanswerStringを呼ぶだけでString型でprintできる
  questionA.answerString();
  questionB.answerString();
}