class Customer {
  final String name;
  final List<Customer> descendant = [];

  Customer({this.name});

  void add(Customer descendant){
    this.descendant.add(descendant);
  }

  bool hasDescendants(){
    return descendant.length > 0;
  }

  // イテレーター使った書き方
  void deleteByIterator(){
    Iterator iterator = descendant.iterator;
    while (iterator.moveNext()) {
      iterator.current.deleteByIterator();
    }
    print('${this.name}を削除します');
  }

  // for文使った書き方
  void deleteByFor(){
    for(Customer d in descendant){
      d.deleteByFor();
    }
    print('${this.name}を削除します');
  }
}



void main() {
  Customer school1 = Customer(name: '教室1');
  Customer school2 = Customer(name: '教室2');
  Customer school3 = Customer(name: '教室3');
  Customer school4 = Customer(name: '教室4');
  Customer area1 = Customer(name: '地域1');
  Customer area2 = Customer(name: '地域2');
  Customer customer = Customer(name: '法人');

  area1.add(school1);
  area1.add(school2);
  area2.add(school3);
  area2.add(school4);
  customer.add(area1);
  customer.add(area2);

  print('=====');
  area2.deleteByIterator();
  print('=====');
  customer.deleteByFor();
}