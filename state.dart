// Moede用のEnum
enum Difficulty {
  NORMAL,
  HARD
}

// モードの振る舞いを持つ抽象クラス
abstract class State {
  Program getFirstQuestion(List<Program> programList){}
}

// ノーマルモードの時の振る舞い
class NormalState implements State {
  Program getFirstQuestion(List<Program> programList){
    return programList[0];
  }
}

// ハードモードの時の振る舞い
class HardState implements State {
  Program getFirstQuestion(List<Program> programList){
    return programList.firstWhere((program) => program.difficulty == Difficulty.HARD);
  }
}

// 問題一覧を管理するクラス
class ProgramList {
  final List<Program> programList;
  State state = NormalState();

  ProgramList({this.programList});

  Program getFirstQuestion() => state.getFirstQuestion(this.programList);
}

class Program {
  final int id;
  final Difficulty difficulty;

  Program({
    this.id,
    this.difficulty
  });
}

void main() {
  // ノーマルモードの問題
  Program program1 = Program(id: 1, difficulty: Difficulty.NORMAL);
  // ハードモードの問題
  Program program2 = Program(id: 2, difficulty: Difficulty.HARD);
  // 2つの問題をリスト化する
  List<Program> programList = [program1, program2]

  // ノーマルモード
  ProgramList programList = ProgramList(programList: programList);
  print(programList.getFirstQuestion().id);

  // ハードモード
  // stateを書き換える
  programList.state = HardState();
  print(programList.getFirstQuestion().id);
}