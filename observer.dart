abstract class Observer {
  void update(Player player, bool isCorrect){}
}

// playLogを管理するクラス
class PlayLog implements Observer{
  List<Map<String, dynamic>> logs = [];

  void update(Player player, bool isCorrect){
    Map<String, dynamic> log = {
      'name': player.name,
      'isCorrect': isCorrect,
    };

    this.logs.add(log);
  }

  void postLog(){
    print('post: $logs');
  }
}

// 問題を管理するクラス
class Program implements Observer{
  int playedCount = 0;
  int correctCount = 0;

  void update(Player player, bool isCorrect){
    playedCount += 1;
    if(isCorrect){
      correctCount +=1;
    }
  }

  void showCorrectRate(){
    print('問題の正答率：　${this.playedCount != 0 ? (correctCount/playedCount * 100) : 0}％');
  }
}

// Playerを管理するクラス
class Player {
  final name;
  final List<Observer> ovservers;
  int correctCount = 0;
  int playedCount = 0;

  Player({this.name, this.ovservers});

  void answer(bool isCorrect){
    this.playedCount += 1;
    if(isCorrect){
      this.correctCount += 1;
    }
    this.notifyObservers(isCorrect);
  }

  void notifyObservers(bool isCorrect){
    this.ovservers.forEach((o) => {
      o.update(this, isCorrect)
    });
  }

  void showCorrectRate(){
    print('$nameの正答率：　${this.playedCount != 0 ? (correctCount/playedCount * 100) : 0}％');
  }
}

void main() {
  Program program = Program();
  PlayLog playLog = PlayLog();

  // 千葉
  Player chiba = Player(name: '千葉さん', ovservers: [program, playLog]);
  // りお
  Player rio = Player(name: 'りお', ovservers: [program, playLog]);

  // 千葉さんは正解する
  chiba.answer(true);
  // りおは不正解
  rio.answer(false);
  // 千葉さんの正答率を見る
  chiba.showCorrectRate();
  // りおの正答率を見る
  rio.showCorrectRate();
  // 問題の正答率を見る
  program.showCorrectRate();
  // ログを送信する
  playLog.postLog();
}