// factoryクラス
class Question {
  final int type;
  final int questionId;

  factory Question({int type, int questionId}) {
    if (type == 1) {
      return QuestionType1(questionId: questionId);
    } else {
      return QuestionType2(questionId: questionId);
    }
  }

  Question._internal({this.type, this.questionId});
}

// QTyoe1用のクラス
class QuestionType1 extends Question {
  QuestionType1({questionId, type})
      : super._internal(type: type, questionId: questionId);
}

// Qtype2用のクラス
class QuestionType2 extends Question {
  QuestionType2({questionId, type})
      : super._internal(type: type, questionId: questionId);
}

void main() {
  // typeを渡すと、questionType1にはQuestionクラスではなく、QuestionType1のクラスが入る！
  Question questionType1 = Question(type: 1, questionId: 1);
}