// Questionの抽象クラスを作る
abstract class Question {
  final int questionId;
  final String answer;

  Question({this.questionId, this.answer});

  // 回答するときの処理群
  void answerQuestion(String playerAnswer) {
    // 正誤判定
    _correctAnswer(playerAnswer);
    // ログの送信
    _postPlayLog(playerAnswer);
  }

  // 正誤判定(具象クラスにする時にoverrideする)
  void _correctAnswer (String playerAnswer) {}

  // ログの送信
  void _postPlayLog  (String playerAnswer) {
    Map param = {
      'id': questionId,
      'answer': playerAnswer,
    };
    print('postするよ： $param');
  }
}

// QTyoe1用の具象クラス
class QuestionType1 extends Question{
  QuestionType1({questionId, answer,}) : super(questionId: questionId, answer: answer);

  // Qtype1用の正誤判定ロジックでoverrideする。
  @override
  void _correctAnswer (String playerAnswer) {
    print('Qtype1: ${playerAnswer == answer}');
  }
}

// Qtype2用の具象クラス
class QuestionType2 extends Question{
  QuestionType2({questionId, answer}) : super(questionId: questionId, answer: answer);

  // Qtype1用の正誤判定ロジックでoverrideする。
  @override
  _correctAnswer (String playerAnswer) {
    print('Qtype2: ${playerAnswer == answer}');
  }
}

void main() {
  // Qtype1で回答
  final questionType1 = QuestionType1(questionId: 1, answer: "aaa");
  questionType1.answerQuestion('aaa');

  // Qtype2で回答
  final questionType2 = QuestionType2(questionId: 1, answer: "aaa");
  questionType2.answerQuestion('aaa');
}