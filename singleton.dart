// singletonクラス
class Api {
  static final String host = 'http://XXXX.com';

  static Api _instance; // インスタンスのキャッシュ

  // 初めて呼び出されたときはインスタンスを生成してキャッシュし、
  // それ以降はキャッシュを返すfactoryコンストラクタ。
  factory Api() {
    if (_instance == null) _instance = Api._internal();
    return _instance;
  }

  // 内部から呼び出してインスタンスを作るための
  // プライベートなコンストラクタ。
  Api._internal();

  // api呼ぶクラス
  void callApi(){
    print('$hostに向かってAPIを呼んだよ');
  }
}

void main() {
  // singleton
  Api apiInstance = Api();
  apiInstance.callApi();
}