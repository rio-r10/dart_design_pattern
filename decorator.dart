// Pointの抽象クラス
abstract class Point{
  double getPoint(int difficulty);
}

// 今まで使っていた、普通の問題を解いた時にポイントを計算するクラス
class NormarGamePoint implements Point {
  double getPoint(int difficulty) {
    return 1 * (1 + (difficulty / 10));
  }
}

// ボーナス問題を解いた時にポイントを計算するクラス
class BornasGamePoint implements Point {
  final NormarGamePoint normalGamePoint;
  BornasGamePoint({ this.normalGamePoint });

  double getPoint(int difficulty) {
    double normalPoint = normalGamePoint.getPoint(difficulty);
    return normalPoint + 5;
  }
}

void main() {
  Point normalGamePoint = NormarGamePoint();
  Point bornasGamePoint = BornasGamePoint(normalGamePoint: normalGamePoint);

  // ノーマルモードで難易度2の問題を解く => 得点は1.2
  print(normalGamePoint.getPoint(2));
  // ボーナスモードで難易度2の問題を解く　=> 合計ポイントは1.2+5=6.2
  print(bornasGamePoint.getPoint(2));
}
