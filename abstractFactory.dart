// 問題の抽象クラス
abstract class Question {
  final int id;

  Question({
    this.id,
  });
}

// 回答が1つの問題
class StringQuestion extends Question {
  StringQuestion({id}) : super(id: id);
}

// 回答が複数の問題
class ListQuestion extends Question {
  ListQuestion({id}) : super(id: id);
}


// プレーする人の抽象クラス
abstract class Person {
  final String name;

  Person({
    this.name
  });
}

// 子供
class Child extends Person {
  Child({name}) : super(name: name);
}

// 大人
class Parent extends Person {
  Parent({name}) : super(name: name);
}


// PlayLog
class PlayLog {
  Question question;
  Person person;

  void setPlayer({String type, name}) {
    if(type=='大人'){
      this.person = Parent(name: name);
    }else {
      this.person = Child(name: name);
    }
  }

  void setQuestion({bool multiple, id}) {
    if(multiple){
      this.question = ListQuestion();
    } else {
      this.question = StringQuestion();
    }
    this.question = question;
  }
}


void main() {
  // プレイログがある
  PlayLog playLog = PlayLog();
  // プレイヤーを設定
  playLog.setPlayer(type: '大人', name: '名前');
  // 問題を選択
  playLog.setQuestion(multiple: true);

}