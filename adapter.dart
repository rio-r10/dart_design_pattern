// 元々あったクラス
class Student {
  final DateTime birthDay;

  Student({ this.birthDay });

  // 生年月日から現在の年齢を出すロジック
  int getAge(){
    DateTime today = DateTime.now();
    int age = today.year - this.birthDay.year;

    if(this.birthDay.month > today.month){
      age = age - 1;
    } else if (today.month == this.birthDay.month && this.birthDay.day > today.day){
      age = age - 1;
    }

    return age;
  }
}

// 継承で作るパターン
// 新しく作る、生年月日から未成年かどうかを判定するクラス
class NoMinorsKeisho extends Student {
  NoMinorsKeisho({birthDay}): super(birthDay: birthDay);

  bool isAdult() {
    int age = this.getAge();
    return age >= 20;
  }
}

// 移譲で作るパターン
class NoMinorsIjo {
  final Student student;

  NoMinorsIjo({ this.student });


  bool isAdult() {
    int age = this.student.getAge();
    return age >= 20;
  }
}

void main() {
  // 継承
  NoMinorsKeisho personKeisho = NoMinorsKeisho(birthDay: DateTime(1989, DateTime.december, 17));
  print(personKeisho.isAdult());

  // 移譲
  NoMinorsIjo personIjo = NoMinorsIjo(student: Student(birthDay: DateTime(1989, DateTime.december, 17)));
  print(personIjo.isAdult());
}